package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SingletonDependsOnPrototypeProxyFactory {

    @Bean
    SingletonDependsOnPrototypeProxy create(PrototypeDependentWithProxy prototypeDependentWithProxy){
        return new SingletonDependsOnPrototypeProxy(prototypeDependentWithProxy);
    }
}
