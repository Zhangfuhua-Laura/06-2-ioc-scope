package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PrototypeDependsSingleton {
    private SimpletonDepend simpletonDepend;

    public PrototypeDependsSingleton(SimpletonDepend simpletonDepend) {
        this.simpletonDepend = simpletonDepend;
    }

    public PrototypeDependsSingleton() {
    }

    public SimpletonDepend simpletonDepend() {
        return simpletonDepend;
    }
}
