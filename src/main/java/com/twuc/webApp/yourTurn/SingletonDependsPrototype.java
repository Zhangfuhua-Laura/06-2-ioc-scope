package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsPrototype {

    private PrototypeDepends prototypeDepends;

    public SingletonDependsPrototype(PrototypeDepends prototypeDepends) {
        this.prototypeDepends = prototypeDepends;
    }

    public SingletonDependsPrototype() {
    }

    public PrototypeDepends getPrototypeDepends() {
        return prototypeDepends;
    }
}
