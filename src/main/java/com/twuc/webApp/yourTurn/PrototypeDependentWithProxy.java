package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {
    private int count;

    public PrototypeDependentWithProxy() {
        count = new Random().nextInt();
    }

    public int getCount() {
        return count;
    }
}
