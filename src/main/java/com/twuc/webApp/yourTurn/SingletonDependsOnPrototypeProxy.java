package com.twuc.webApp.yourTurn;

public class SingletonDependsOnPrototypeProxy {

    private PrototypeDependentWithProxy prototypeDependentWithProxy;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
    }

    public SingletonDependsOnPrototypeProxy() {
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public boolean batchCall(){
        String str1 = this.getPrototypeDependentWithProxy().toString();
        String str2 = this.getPrototypeDependentWithProxy().toString();
        return str1 == str2 ? true : false;
    }
}

