package Singleton;

import org.springframework.stereotype.Component;

@Component
public class SingletonScopeClass {
    public SingletonScopeClass() {
        throw new RuntimeException("Create a SingletonScopeClass");
    }
}
