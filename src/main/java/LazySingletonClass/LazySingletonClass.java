package LazySingletonClass;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class LazySingletonClass {
    public LazySingletonClass() {
        throw new RuntimeException("I am lazy so I am created when get Bean");
    }
}
