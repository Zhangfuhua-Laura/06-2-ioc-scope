package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertSame;

public class SingletonTest {

    @Test
    void should_get_the_same_class_when_get_the_Interface_and_InterfaceImpl(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        InterfaceOneImpl interfaceOneImpl = context.getBean(InterfaceOneImpl.class);
        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);
        assertSame(interfaceOne, interfaceOneImpl);
    }

    @Test
    void should_get_the_same_class_when_get_the_Inherit_and_InheritExtends(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Inherit inherit = context.getBean(Inherit.class);
        InheritExtends inheritExtends = context.getBean(InheritExtends.class);
        assertSame(inherit, inheritExtends);
    }

    @Test
    void should_get_the_same_class_when_get_the_abstract_base_class_and_derived_class(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstractBaseClass, derivedClass);
    }
}
