package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class ProxyModeTest {

    @Test
    void should_create_a_new_prototype_depend_when_create_singleton_every_time(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonDependsOnPrototypeProxy singletonDependsOnPrototypeProxyOne = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy singletonDependsOnPrototypeProxyAnother = context.getBean(SingletonDependsOnPrototypeProxy.class);

        int count1 = singletonDependsOnPrototypeProxyOne.getPrototypeDependentWithProxy().getCount();
        int count2 = singletonDependsOnPrototypeProxyAnother.getPrototypeDependentWithProxy().getCount();

        assertNotEquals(count1, count2);

        assertSame(singletonDependsOnPrototypeProxyOne, singletonDependsOnPrototypeProxyAnother);
    }

    @Test
    void should_create_two_prototype_depend_when_call_the_prototype_method_twice(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonDependsOnPrototypeProxy singletonDependsOnPrototypeProxy = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertFalse(singletonDependsOnPrototypeProxy.batchCall());
    }

}
