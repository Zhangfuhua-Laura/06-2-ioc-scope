package com.twuc.webApp.yourTurn;

import LazySingletonClass.LazySingletonClass;
import Prototype.PrototypeScopeClass;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class PrototypeTest {
    @Test
    void should_get_different_class_when_get_two_prototype_beans(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SimpletonDepend simpleProtoTypeScopeClassOne = context.getBean(SimpletonDepend.class);
        SimpletonDepend simpleProtoTypeScopeClassAnother = context.getBean(SimpletonDepend.class);
        assertNotSame(simpleProtoTypeScopeClassOne, simpleProtoTypeScopeClassAnother);
    }

    @Test
    void should_create_singleton_class_when_create_context(){
        try{
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("Singleton");
        }catch (Exception e){
            return;
        }
        fail();
    }

    @Test
    void should_create_prototype_class_when_create_getBean(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("Prototype");
        try{
            PrototypeScopeClass prototypeScopeClass = context.getBean(PrototypeScopeClass.class);
        }catch (Exception e){
            return;
        }
        fail();
    }

    @Test
    void should_create_singleton_class_when_get_bean_using_lazy(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("LazySingletonClass");
        try{
            LazySingletonClass lazySingletonClass = context.getBean(LazySingletonClass.class);
        }catch (Exception e){
            return;
        }
        fail();
    }
    @Test
    void should_create_two_prototype_class_and_one_singleton_class_when_create_a_prototype_dependent_singleton_class_twice(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        PrototypeDependsSingleton prototypeDependsSingletonOne = context.getBean(PrototypeDependsSingleton.class);
        PrototypeDependsSingleton prototypeDependsSingletonAnother = context.getBean(PrototypeDependsSingleton.class);

        assertNotSame(prototypeDependsSingletonOne, prototypeDependsSingletonAnother);
        assertSame(prototypeDependsSingletonOne.simpletonDepend(), prototypeDependsSingletonAnother.simpletonDepend());
    }

    @Test
    void should_create_one_prototype_class_and_one_singleton_class_when_create_a_singleton_dependent_prototype_class_twice(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonDependsPrototype singletonDependsPrototypeOne = context.getBean(SingletonDependsPrototype.class);
        SingletonDependsPrototype singletonDependsPrototypeAnother = context.getBean(SingletonDependsPrototype.class);

        assertSame(singletonDependsPrototypeOne, singletonDependsPrototypeAnother);
        assertSame(singletonDependsPrototypeOne.getPrototypeDepends(), singletonDependsPrototypeAnother.getPrototypeDepends());
    }
}
